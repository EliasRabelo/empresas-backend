const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();
const app = require('../index')
const expect = require('chai').expect;
chai.use(chaiHttp);
const {uid,client,access_token} = require('./tokens.json')

describe('enterprises', ()=>{
    it('shoul list all enterprises',async ()=>{
        const res = await chai.request(app)
        .get('/api/v1/enterprises')
        .set({'client':client,
            'uid':uid,
            'access-token':access_token})
        expect(res).to.have.status(200);
        expect(res.body).to.have.property('enterprises');
    })
    it('should list all enterprises with filters',async ()=>{
        const res = await chai.request(app)
        .get('/api/v1/enterprises?enterprise_types=1&name=aQm')
        .set({'client':client,
        'uid':uid,
        'access-token':access_token})
        expect(res).to.have.status(200);
        expect(res.body).to.have.property('enterprises');
    })
    it('should find enterprise by id',async ()=>{
        const res = await chai.request(app)
        .get('/api/v1/enterprises/1')
        .set({'client':client,
            'uid':uid,
            'access-token':access_token})
        expect(res).to.have.status(200);
        expect(res.body).to.have.property('enterprise');
        expect(res.body).to.have.property('success')
    })
    app.stop();
})