const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();
const app = require('../index');
const fs = require('fs');
chai.use(chaiHttp);



describe('auth',()=>{
    describe('POST => /api/v1/users/auth/sign_in',()=>{
        it('Should login a user',(done)=>{
            chai.request(app)
            .post('/api/v1/users/auth/sign_in').
            send({
              email:'lucasrizel@ioasys.com.br',
              password:'12345678'
            }).end((err,res)=>{
              res.should.have.status(200);
              res.body.should.have.property('investor');
              res.body.should.have.property('enterprise');
              res.body.should.have.property('success');
              done()
            }) 
        })
    })
})
