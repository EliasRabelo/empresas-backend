const express = require('express');
const models = require('./app/models');
const bodyParser = require('body-parser')
const app = express();

const enterprisesRouter = require('./app/routers/enterprises-router');
const usersRouter = require('./app/routers/auth-router');
const base_path = '/api/v1';

app.use(express.urlencoded({ extended: false }));

app.use(bodyParser.json())
app.use(base_path+'/enterprises',enterprisesRouter());
app.use(base_path+'/users',usersRouter());
const server = app.listen(3000,()=>{
  console.log('listing on port 3000')
});
function stop() {
  server.close();
}


module.exports = app;
module.exports.stop = stop;