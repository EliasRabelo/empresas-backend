const models = require('./app/models');
const Investor = models.investor;
const EnterpriseType = models.enterprise_type;
const Enterprise = models.enterprise;


// arquivo usado apenas para popular o banco

Investor.bulkCreate([
    {email:'lucasrizel@ioasys.com.br',
    password:'$2b$10$VHqJ8UY3iNDZ.F9ZnV0a4.SXU.Rnt9varFpEijW2vY5CF0h17e.1a',
    investor_name:'Teste Apple',
    city:'BH',
    country:'Brasil',
    balance:10000,
    photo:null,
    portfolio_value:10000,
    first_access:false,
    super_angel:false
}
]).then((investors)=>{
    console.log(investors);
}).catch((err)=>{
    console.log(err)
})

EnterpriseType.bulkCreate([
    {enterprise_type_name:'Agro'},
    {enterprise_type_name:'Software'}
]).then((types)=>{
    console.log(types);
}).catch((err)=>{
    console.log(err)
})

Enterprise.bulkCreate([{
        enterprise_name:"AllRide",
        description:"Urbanatika is a socio-environmental company with economic impact, creator of the agro-urban industry. We want to involve people in the processes of healthy eating, recycling and reuse of organic waste and the creation of citizen green areas. With this we are creating smarter cities from the people and at the same time the forest city.  Urbanatika, Agro-Urban Industry",
        email_enterprise:"",
        facebook:"",
        twitter:"",
        linkedin:"",
        phone:"",
        own_enterprise:false,
        photo:"/uploads/enterprise/photo/1/wood_trees_gloomy_fog_haze_darkness_50175_1920x1080.jpg",
        value:0,
        shares:100,
        share_price:10000.0,
        own_shares:0,
        city:"Santiago",
        country:"Chile",
        enterpriseTypeId:2},

        {
            email_enterprise:null,
            facebook:null,
            twitter:null,
            linkedin:null,
            phone:null,
            own_enterprise:false,
            enterprise_name:"AQM S.A.",
            photo:null,
            description:"Cold Killer was discovered by chance in the ´90 s and developed by Mrs. Inés Artozon Sylvester while she was 70 years old. Ending in a U.S. patent granted and a new company, AQM S.A. Diluted in water and applied to any vegetable leaves, stimulate increase glucose (sugar) up to 30% therefore help plants resists cold weather. ",
            city:"Maule",
            country:"Chile",
            value:0,
            share_price:5000.0,
            enterpriseTypeId:1
        },
        {
        email_enterprise:null,
        facebook:null,
        twitter:null,
        linkedin:null,
        phone:null,
        own_enterprise:false,
        enterprise_name:"urbanatika",
        photo:null,
        description:"Urbanatika is a socio-environmental company with economic impact, creator of the agro-urban industry. We want to involve people in the processes of healthy eating, recycling and reuse of organic waste and the creation of citizen green areas. With this we are creating smarter cities from the people and at the same time the forest city.  Urbanatika, Agro-Urban Industry",
        city:"santiago",
        country:"Chile",
        value:0,
        share_price:5000.0,
        enterpriseTypeId:1
    }

]).then((enterprises)=>{
    console.log(enterprises);
}).catch((err)=>{
    console.log(err)
})