const models = require('../models');
const Investor = models.investor;
const bcrypt = require('bcrypt');
const seeds = require ('random-seed').create();
const jwt = require('jsonwebtoken');
const SECRET = require('../config/secret.json');

const sign_in = async(req,res) =>{

 try{
    const {email,password} = req.body;
    const investor = await Investor.findOne({where:{
        email:email
    }})
    if(!investor){
        return res.status(401).send({
            sucess: false,
            error: ['Invalid login credentials. Please try again'],
          });
    }

    if(!await bcrypt.compare(password,investor.password)) return res.status(401).send({
        sucess: false,
        error: ['Invalid login credentials. Please try again'],
      })
    
    // Não acredito que seja necessario esse passo,
    // mas o fiz pra manter a ordem exata como na tabela do exemplo.
    
    const {id,
    investor_name,
    city,
    country,
    balance,
    photo,
    portfolio_value,
    first_access,
    super_angel    
    } = investor
 
    res.setHeader('uid',investor.email);
    res.setHeader('client',seeds(35)) // gerei um numero por nao saber como os clients devem ser gerados.
    const token = jwt.sign({id:investor.id},
        SECRET.secret,
        {expiresIn:86400})
    res.setHeader('access-token',token);
 
    return res.send({investor:{
                id,
                investor_name,
                email,
                city,
                country,
                balance,
                photo,
                portfolio:{
                    enterprises_number:0, // Dada a falta de clareza do que é um portifolio eu decidir enviar como um campo mocado
                    enterprises:[] // exatamente igual ao exemplo do servidor de teste;
                },
                portfolio_value,
                first_access,
                super_angel   
         },
        enterprise:null, // como não é posivel inferir a relacao entre investor e enterprise, mative esse campo como null, assim como no exemplo
        success:true})

    }catch(err){
    // deverar fazer um tratamento mais adequado do error.
    res.status(400).send({error:'internal server error'})
}
}

module.exports = {sign_in}