'use strict'
const models = require('../models');
const Enterprises = models.enterprise;
const Sequelize = require('sequelize');
const Op = Sequelize.Op;

const getEnterprises = async (req,res )=>{
    try {
       const {name,enterprise_types} = req.query;
       if(name||enterprise_types){
           let where = new Object();
            if(name) where.enterprise_name = {
                [Op.like]:`%${name}%`
            }
            if(enterprise_types) where.enterpriseTypeId=enterprise_types
            const enterprises = await Enterprises.findAll({
                where:where
                ,include:['enterprise_type'],
                attributes:{exclude:['enterpriseTypeId','own_shares','shares']}
            })
          return res.send({enterprises:enterprises})
       }      
       
       const enterprises = await Enterprises.findAll(
           {include:['enterprise_type'],
           attributes:{exclude:['enterpriseTypeId','own_shares','shares']}
        }
       );
      return  res.send({enterprises:enterprises})
    }catch(err){
      
        return res.status(500).send({msg: 'internal server error'})
    }
  

}

const getEnterpriseById = async(req,res) => {

    try{
 
        const {id} = req.params;
        if(isNaN(id)){
            return res.status(400).send({error:'id must be a number'})
        }



        const enterprise = await Enterprises.findByPk(id,
            {include:['enterprise_type'],attributes:{exclude:['enterpriseTypeId']}}
        )

        if(!enterprise) return res.status(404).send({
            status:'404',
            error:'Not Found'
        })
         /* eu não vejo a destruturação do objeto enterprise feita a seguir como uma estrategia valida,
        no entendo percebi que a saida do endpoint show tem os campos de ordem diferentes dos outros endpoints,
        mesmo sendo os mesmos campos basicamente. A ordem dos campos é irrelevante para a execução dos clientes usando a api,
        desde que os campos sejam iguais, no entanto para cumprir a saida a risca do desafio decidi fazer.
        */
       const {
        enterprise_name,
        description,
        email_enterprise,
        facebook,
        twitter,
        linkedin,
        phone,
        own_enterprise,
        photo,
        value,
        shares,
        share_price,
        own_shares,
        city,
        country,
        enterprise_type
        } = enterprise;

        return res.send({
            enterprise:{
            id,
            enterprise_name,
            description,
            email_enterprise,
            facebook,
            twitter,
            linkedin,
            phone,
            own_enterprise,
            photo,
            value,
            shares,
            share_price,
            own_shares,
            city,
            country,
            enterprise_type
            },
            success:true
        })
    }catch(err){

        return res.status(500).send({msg: 'internal server error'})
    }
}

module.exports = {getEnterprises,getEnterpriseById}