'use strict'
const router = require('express').Router();
const enterprisesControlller = require('../controllers/enterprises-controller')
const enterprisesMiddleware = require('../middlewares/enterprises-middleware')
const authMiddleware = require('../middlewares/auth-middleware');

router.use(enterprisesMiddleware);
router.use(authMiddleware);

const enterprisesRouter = () =>{
    router.route('/')
        .get(enterprisesControlller.getEnterprises)
    router.route('/:id')
        .get(enterprisesControlller.getEnterpriseById)
    return router;
}

module.exports = enterprisesRouter