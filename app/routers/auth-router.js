'use strict'
const router = require('express').Router();
const authController = require('../controllers/auth-controller')

const authRouter = () =>{
    router.route('/auth/sign_in')
    .post(authController.sign_in)
    return router;
}

module.exports = authRouter