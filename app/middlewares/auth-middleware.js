const jwt = require('jsonwebtoken');
const SECRET = require('../config/secret.json');
const ERROR = {
    "errors": [
        "You need to sign in or sign up before continuing."
    ]
}
module.exports = async(req,res,next) => {
    // Separado em tres if pq em um cenario real cada um seria tratado com um erro.
    if(!req.headers.hasOwnProperty('client')) return res.status(400).send(ERROR)
    if(!req.headers.hasOwnProperty('uid')) return res.status(400).send(ERROR)
    if(!req.headers.hasOwnProperty('access-token')) return res.status(400).send(ERROR)
    
    
    jwt.verify(req.headers['access-token'],SECRET.secret,(err,decode)=>{
        if(err) return res.status(400).send(ERROR);
        req.userId=decode.id;
        res.setHeader('client',req.headers['client'])
        res.setHeader('uid',req.headers['uid'])
        res.setHeader('access-token',req.headers['access-token'])
        next();
    })
}