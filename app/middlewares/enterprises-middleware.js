

// middleware simples pra impedir o uso de tipos errados nas requisicoes
module.exports = async (req,res,next)=>{
     const {name,enterprise_types} = req.query
     if(name&& typeof name !== 'string' ){
        return res.status(400).send({msg:'name param is not a sting'})
     }
     if(enterprise_types&& isNaN(enterprise_types)){
         return res.status(400).send({error:'enterprise_types must be number'})
     }

     next()
}