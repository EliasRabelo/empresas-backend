'use strict';
module.exports = (sequelize, DataTypes) => {
  const enterprise = sequelize.define('enterprise', {
    email_enterprise: DataTypes.STRING,
    facebook: DataTypes.STRING,
    twitter: DataTypes.STRING,
    linkedin: DataTypes.STRING,
    phone: DataTypes.STRING,
    own_enterprise: DataTypes.BOOLEAN,
    enterprise_name:DataTypes.STRING,
    photo: DataTypes.STRING,
    description: DataTypes.STRING,
    city:DataTypes.STRING,
    country: DataTypes.STRING,
    value: DataTypes.INTEGER,
    shares:DataTypes.FLOAT,
    share_price: DataTypes.FLOAT,
    own_shares:DataTypes.FLOAT,
    enterpriseTypeId: DataTypes.INTEGER
  }, {timestamps: false});
  enterprise.associate = function(models) {
    enterprise.belongsTo(models.enterprise_type,{foreignKey:'enterpriseTypeId',as:'enterprise_type'})
  };
  return enterprise;
};