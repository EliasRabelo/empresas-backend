'use strict';
module.exports = (sequelize, DataTypes) => {
  const enterprise_type = sequelize.define('enterprise_type', {
    enterprise_type_name: DataTypes.STRING
  }, {timestamps: false});
  enterprise_type.associate = function(models) {
    // associations can be defined here
  };
  return enterprise_type;
};