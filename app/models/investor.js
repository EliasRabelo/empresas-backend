'use strict';
module.exports = (sequelize, DataTypes) => {
  const investor = sequelize.define('investor', {
    investor_name: DataTypes.STRING,
    email: DataTypes.STRING,
    city: DataTypes.STRING,
    country: DataTypes.STRING,
    balance: DataTypes.FLOAT,
    photo: DataTypes.STRING,
    portfolio_value: DataTypes.FLOAT,
    first_access: DataTypes.BOOLEAN,
    super_angel: DataTypes.BOOLEAN,
    password: DataTypes.STRING
  }, {timestamps: false});
  investor.associate = function(models) {
    // associations can be defined here
  };
  return investor;
};