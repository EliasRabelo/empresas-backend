'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('enterprises', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      email_enterprise: {
        type: Sequelize.STRING,
        unique:true
      },
      facebook: {
        type: Sequelize.STRING
      },
      twitter: {
        type: Sequelize.STRING
      },
      linkedin: {
        type: Sequelize.STRING
      },
      phone: {
        type: Sequelize.STRING,
        unique:true
      },
      own_enterprise: {
        type: Sequelize.BOOLEAN
      },
      enterprise_name:{
        type:Sequelize.STRING,
        allowNull:false
      },
      photo: {
        type: Sequelize.STRING
      },
      description: {
        type: Sequelize.TEXT
      },
      city:{
        type:Sequelize.STRING
      },
      country: {
        type: Sequelize.STRING
      },
      value: {
        type: Sequelize.INTEGER
      },
      shares:{
        type:Sequelize.INTEGER
      },
      share_price: {
        type: Sequelize.FLOAT
      },
      own_shares:{
        type:Sequelize.FLOAT
      },  
      enterpriseTypeId: {
        type: Sequelize.INTEGER,
        allowNull:false,
        references:{
          model:'enterprise_types',
          key:'id'
        }
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('enterprises');
  }
};